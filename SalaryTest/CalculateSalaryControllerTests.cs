using System.Data;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using OvetimePolicies;
using SalaryApi.Controllers;
using SalaryApi.Data;
using SalaryApi.Dto;
using SalaryApi.Model;

namespace SalaryApi.Tests
{
    [TestFixture]
    public class CalculateSalaryControllerTests
    {
        private CalculateSalaryController _controller;
        private Mock<ICalculator> _mockCalculator;
        private SalaryDbContext _dbContext;
        private Mock<SalaryDbContext> _mockDbContext;
        private Mock<SalaryDapperContext> _mockDapperContext;
        private IConfiguration _mockConfiguration;

        [SetUp]
        public void Setup()
        {
            _mockCalculator = new Mock<ICalculator>();
            _dbContext = GetInMemoryDbContext();
            _mockDbContext = new Mock<SalaryDbContext>();
            _mockDapperContext = new Mock<SalaryDapperContext>(_mockConfiguration);
            _mockConfiguration = new ConfigurationBuilder().AddInMemoryCollection().Build();

            var mockConnection = new Mock<IDbConnection>();
            _mockDapperContext.Setup(d => d.CreateConnection()).Returns(mockConnection.Object);

            _controller = new CalculateSalaryController(
                _mockCalculator.Object,
                _dbContext,
                _mockDapperContext.Object
            );
        }

        private SalaryDbContext GetInMemoryDbContext()
        {
            var options = new DbContextOptionsBuilder<SalaryDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDb")
                .Options;

            var configuration = CreateConfiguration();

            var dbContext = new SalaryDbContext(configuration);
            dbContext.Database.EnsureCreated();

            return dbContext;
        }

        private IConfiguration CreateConfiguration()
        {
            return new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string>
                {
                    {"SalaryApiDatabase", "Data Source=:memory:"}
                })
                .Build();
        }

        private IDbConnection GetInMemoryConnection()
        {
            var connection = new SqliteConnection("Data Source=:memory:");
            connection.Open();
            return connection;
        }

        [Test]
        public async Task GetRage_ReturnsOkResultWithResultData()
        {
            // Arrange
            DateTimeOffset fromDate = new DateTimeOffset(2023, 7, 1, 0, 0, 0, TimeSpan.Zero);
            DateTimeOffset toDate = new DateTimeOffset(2023, 7, 31, 0, 0, 0, TimeSpan.Zero);
            var mockUserSalaries = new List<UserSalary>
            {
                new UserSalary { Id = Guid.NewGuid(), FirstName = "John", LastName = "Doe", FromDate = fromDate, ToDate = toDate, Salary = 5000 },
                new UserSalary { Id = Guid.NewGuid(), FirstName = "Jane", LastName = "Smith", FromDate = fromDate, ToDate = toDate, Salary = 6000 }
            };

            // Mock the Dapper context to return the mockUserSalaries
            var mockConnection = new Mock<IDbConnection>();
            mockConnection.Setup(conn => conn.Query<UserSalary>(It.IsAny<string>(), It.IsAny<object>(), null, It.IsAny<bool>(), It.IsAny<int>(), null))
                            .Returns(mockUserSalaries);

            _mockDapperContext.Setup(d => d.CreateConnection()).Returns(mockConnection.Object);

            // Act
            var result = await _controller.GetRage(fromDate, toDate);

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
            var okResult = result as OkObjectResult;
            Assert.IsInstanceOf<IEnumerable<UserSalary>>(okResult.Value);
            var userSalaries = okResult.Value as IEnumerable<UserSalary>;
            Assert.AreEqual(2, userSalaries.Count());
        }
        [Test]
        public async Task Get_ExistingEntity_ReturnsOkResultWithEntity()
        {
            // Arrange
            var entityId = Guid.NewGuid();
            var mockUserSalary = new UserSalary { Id = entityId, FirstName = "John", LastName = "Doe", Salary = 5000 };
            var mockUserSalaries = new List<UserSalary> { mockUserSalary };

            // Mock the Dapper context to return the mockUserSalaries
            var mockConnection = new Mock<IDbConnection>();
            mockConnection.Setup(conn => conn.Query<UserSalary>(It.IsAny<string>(), It.IsAny<object>(), null, It.IsAny<bool>(), It.IsAny<int>(), null))
                            .Returns(mockUserSalaries);

            _mockDapperContext.Setup(context => context.CreateConnection()).Returns(mockConnection.Object);

            // Act
            var result = await _controller.Get(entityId);

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
            var okResult = result as OkObjectResult;
            Assert.AreEqual(mockUserSalary, okResult.Value);
        }

        [Test]
        public async Task Get_NonExistingEntity_ReturnsNotFoundResult()
        {
            // Arrange
            var nonExistingEntityId = Guid.NewGuid();
            var mockUserSalaries = new List<UserSalary>(); // Empty list to simulate no data

            var mockConnection = new Mock<IDbConnection>();
            mockConnection.Setup(conn => conn.Query<UserSalary>(It.IsAny<string>(), It.IsAny<object>(), null, It.IsAny<bool>(), It.IsAny<int>(), null))
                            .Returns(mockUserSalaries);

            _mockDapperContext.Setup(context => context.CreateConnection()).Returns(mockConnection.Object);

            // Act
            var result = await _controller.Get(nonExistingEntityId);

            // Assert
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task Add_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var addRequest = new AddRequest
            {
                Data = "Mojtaba/Morsali/10000/300/600/14000306",
                OverTimeCalculator = "CalculatorA"
            };

            // Mock the ICalculator for the specified method
            _mockCalculator.Setup(calculator => calculator.CalcurlatorA(10000, 300)).Returns(200);

            // Act
            var result = await _controller.Add(addRequest);

            // Assert
            Assert.IsInstanceOf<OkResult>(result);
            _mockDbContext.Verify(db => db.Add(It.IsAny<UserSalary>()), Times.Once);
        }

        private CalculateRequest GetMockCalculateRequest(string data)
        {
            var reqArr = data.Split('/');
            CalculateRequest reqModel = new CalculateRequest
            {
                FirstName = reqArr[0],
                LastName = reqArr[1],
                BasicSalary = Convert.ToDecimal(reqArr[2]),
                Allowance = Convert.ToDecimal(reqArr[3]),
                Transportation = Convert.ToDecimal(reqArr[4]),
                Date = DateTimeOffset.Parse(reqArr[5]), // Changed from DateHelper.GenerateDateFromShamsiDate(reqArr[5]),
                ShamsiDate = reqArr[6]
            };

            return reqModel;
        }

        [Test]
        public async Task Delete_ExistingEntity_ReturnsOkResult()
        {
            // Arrange
            var entityId = Guid.NewGuid();
            var existingUserSalary = new UserSalary { Id = entityId, FirstName = "John", LastName = "Doe", Salary = 5000 };
            _mockDbContext.Setup(db => db.UserSalaries.FirstOrDefault(us => us.Id == entityId)).Returns(existingUserSalary);
            _mockDbContext.Setup(db => db.Remove(It.IsAny<UserSalary>()));

            // Act
            var result = await _controller.Delete(entityId);

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
            _mockDbContext.Verify(db => db.Remove(It.IsAny<UserSalary>()), Times.Once);
        }

        [Test]
        public async Task Delete_NonExistingEntity_ReturnsNotFoundResult()
        {
            // Arrange
            var nonExistingEntityId = Guid.NewGuid();
            _mockDbContext.Setup(db => db.UserSalaries.FirstOrDefault(us => us.Id == nonExistingEntityId)).Returns((UserSalary)null);

            // Act
            var result = await _controller.Delete(nonExistingEntityId);

            // Assert
            Assert.IsInstanceOf<NotFoundResult>(result);
            _mockDbContext.Verify(db => db.Remove(It.IsAny<UserSalary>()), Times.Never);
        }

    }
}