namespace SalaryApi.Dto;

public class UpdateRequest
{
    public Guid Id { get; set; }
    public decimal NewSalary { get; set; }
}
