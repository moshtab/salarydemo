namespace SalaryApi.Dto;

public class AddRequest
{
    public string Data { get; set; }
    public string OverTimeCalculator { get; set; }
}