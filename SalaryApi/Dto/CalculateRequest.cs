namespace SalaryApi.Dto;

public class CalculateRequest
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public decimal BasicSalary { get; set; }
    public decimal Allowance { get; set; }
    public decimal Transportation { get; set; }
    public DateTimeOffset Date { get; set; }
    public string ShamsiDate { get; set; }
}