using System.Globalization;

namespace SalaryApi.Helper;

public static class DateHelper
{
    public static string FormatShamsiDate(string shamsiDate)
    {
        return $"{shamsiDate.Substring(0, 4)}-{shamsiDate.Substring(4, 2)}-{shamsiDate.Substring(6, 2)}";
    }

    public static DateTimeOffset GenerateDateFromShamsiDate(string shamsiDate)
    {
        CultureInfo persianCulture = new CultureInfo("fa-IR");
        DateTimeOffset persianDateTime = DateTimeOffset.ParseExact(FormatShamsiDate(shamsiDate),
            "yyyy-MM-dd", persianCulture);

        return persianDateTime;
    }
}