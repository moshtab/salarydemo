namespace SalaryApi.Model;

public class UserSalary
{
    public Guid Id { get; set; }

    public string FirstName { get; set; }
    public string LastName { get; set; }

    public decimal Salary { get; set; }

    public DateTimeOffset FromDate { get; set; }
    public DateTimeOffset ToDate { get; set; }

    public string ShamsiDate { get; set; }
}