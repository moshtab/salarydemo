using System.Data;
using Dapper;
using Microsoft.Data.Sqlite;

namespace SalaryApi.Data;
public interface ISalaryDapperContext
{
    IDbConnection CreateConnection();
}

public class SalaryDapperContext : ISalaryDapperContext
{
    protected readonly IConfiguration Configuration;

    public SalaryDapperContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IDbConnection CreateConnection()
    {
        return new SqliteConnection(Configuration.GetConnectionString("SalaryApiDatabase"));
    }
}