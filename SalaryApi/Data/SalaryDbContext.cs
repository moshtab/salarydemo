using Microsoft.EntityFrameworkCore;
using SalaryApi.Model;

namespace SalaryApi.Data;

public class SalaryDbContext : DbContext
{
    protected readonly IConfiguration Configuration;

    public SalaryDbContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // connect to sqlite database
        options.UseSqlite(Configuration.GetConnectionString("SalaryApiDatabase"));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UserSalary>();
    }
    public DbSet<UserSalary> UserSalaries { get; set; }
}