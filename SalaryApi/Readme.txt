SalaryApi

SalaryApi is an ASP.NET Core Web API for managing user salaries and calculating overtime payments. The API allows you to add, update, delete, and retrieve user salary information.

Table of Contents
1. Getting Started
   1.1 Prerequisites
   1.2 Installation
2. Usage
3. API Endpoints
4. Testing
5. Docker
6. Contributing
7. License

Getting Started

Prerequisites

Before running the API, make sure you have the following prerequisites installed:

.NET SDK 7

Installation

1. Clone the repository:

git clone https://github.com/your-username/salary-api.git

2. Change directory to the project folder:

cd salary-api

3. Restore NuGet packages:

dotnet restore

4. Build the application:

dotnet build

5. Run the API:

dotnet run

By default, the API will be accessible at http://localhost:5000 or https://localhost:5001 (if using HTTPS).

Usage

To use the API, you can send HTTP requests to the various endpoints listed in the API Endpoints section. The API supports the following operations:

Retrieve a list of user salaries within a date range.
Retrieve a specific user salary by its ID.
Add a new user salary.
Update an existing user salary.
Delete a user salary.

API Endpoints

The API provides the following endpoints:

GET /{dataFormat}/CalculateSalary/GetRage - Retrieve a list of user salaries within a specified date range.
Parameters:
fromDate (optional) - Start date of the range in yyyy-MM-dd format.
toDate (optional) - End date of the range in yyyy-MM-dd format.
Response: Returns a list of user salaries.

GET /{dataFormat}/CalculateSalary/{id} - Retrieve a specific user salary by its ID.
Parameters:
id - The ID of the user salary.
Response: Returns the user salary with the specified ID.

POST /{dataFormat}/CalculateSalary/Add - Add a new user salary.
Request Body: JSON object containing the user salary data.
Response: Returns 200 OK if the user salary is added successfully.

PUT /{dataFormat}/CalculateSalary/{id} - Update an existing user salary by its ID.
Parameters:
id - The ID of the user salary to update.
Request Body: JSON object containing the updated user salary data.
Response: Returns 200 OK if the user salary is updated successfully.

DELETE /{dataFormat}/CalculateSalary/{id} - Delete a user salary by its ID.
Parameters:
id - The ID of the user salary to delete.
Response: Returns 200 OK if the user salary is deleted successfully.

Note: Replace {dataFormat} with the desired data format for the response, such as JSON or XML.

Testing

The API is unit tested using NUnit and Moq. To run the tests, use the following command:

dotnet test

Docker

To containerize the API using Docker, follow the steps in the Dockerfile provided. Build the Docker image and run the container to host the API in a containerized environment.

Contributing

Contributions are welcome! If you find a bug or have a feature request, please open an issue or submit a pull request. Please follow the project's coding style and guidelines.

License

This project is licensed under the MIT License - see the LICENSE file for details.
