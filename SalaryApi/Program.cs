using Microsoft.EntityFrameworkCore;
using OvetimePolicies;
using SalaryApi.Data;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<SalaryDbContext>();
// builder.Services.AddSingleton<SalaryDapperContext>();
builder.Services.AddScoped<ISalaryDapperContext, SalaryDapperContext>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<ICalculator, Calculator>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

using var scope = app.Services.CreateScope();
var context = scope.ServiceProvider.GetRequiredService<SalaryDbContext>();
context.Database.EnsureCreated();
context.Database.Migrate();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
