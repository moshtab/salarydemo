using Dapper;
using Microsoft.AspNetCore.Mvc;
using OvetimePolicies;
using SalaryApi.Data;
using SalaryApi.Dto;
using SalaryApi.Helper;
using SalaryApi.Model;

namespace SalaryApi.Controllers;

[ApiController]
[Route("{dataFormat}/[controller]")]
public class CalculateSalaryController : ControllerBase
{
    public ICalculator _calculator { get; set; }
    private readonly SalaryDbContext _dbContext;
    private readonly ISalaryDapperContext _dapperContext;

    public CalculateSalaryController(ICalculator calculator, SalaryDbContext dbContext, ISalaryDapperContext dapperContext)
    {
        _calculator = calculator;
        _dbContext = dbContext;
        _dapperContext = dapperContext;
    }

    [HttpGet]
    public async Task<IActionResult> GetRage(DateTimeOffset? fromDate, DateTimeOffset? toDate)
    {
        try
        {
            DateTimeOffset defaultFromDate = fromDate ?? DateTimeOffset.MinValue;
            DateTimeOffset defaultEndDate = toDate ?? DateTimeOffset.MaxValue;

            string query = $@"
            SELECT * 
            FROM UserSalaries
            WHERE FromDate >= @FromDate AND FromDate <= @ToDate
            ORDER BY Id";

            using var dbConnection = _dapperContext.CreateConnection();

            var result = dbConnection.Query<object>(query, new { FromDate = defaultFromDate, ToDate = defaultEndDate });

            return Ok(result);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            using var dbConnection = _dapperContext.CreateConnection();

            string query = "SELECT * FROM UserSalaries WHERE Id = @Id";

            var result = dbConnection.QueryFirstOrDefault<UserSalary>(query, new { Id = id });

            if (result == null) return NotFound();

            return Ok(result);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [HttpPost]
    public async Task<IActionResult> Add(AddRequest req)
    {
        try
        {
            var calculateRequest = ConvertCustomeType(req.Data);

            var calculatedOverTime = CalculateBasedOnMethodName(req.OverTimeCalculator, calculateRequest.BasicSalary, calculateRequest.Allowance);

            var UserSalary = new UserSalary
            {
                FirstName = calculateRequest.FirstName,
                LastName = calculateRequest.LastName,
                FromDate = calculateRequest.Date,
                ToDate = calculateRequest.Date.AddMonths(1),
                ShamsiDate = calculateRequest.ShamsiDate,
                Salary = calculateRequest.BasicSalary
                    + calculateRequest.Allowance
                    + calculateRequest.Transportation
                    - calculatedOverTime
                    - (calculateRequest.BasicSalary % 9) // Tax
            };

            _dbContext.Add(UserSalary);
            _dbContext.SaveChanges();
            return Ok();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Update(Guid id, UpdateRequest model)
    {
        try
        {
            var userSalary = _dbContext.UserSalaries.FirstOrDefault(us => us.Id == id);

            if (userSalary == null) return NotFound();
            userSalary.Salary = model.NewSalary;

            _dbContext.Update(userSalary);
            _dbContext.SaveChanges();
            return Ok();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            var userSalary = _dbContext.UserSalaries.FirstOrDefault(us => us.Id == id);
            if (userSalary == null) return NotFound();
            _dbContext.Remove(userSalary);
            _dbContext.SaveChanges();
            return Ok(new { message = "User deleted" });
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    private CalculateRequest ConvertCustomeType(string req)
    {
        var reqArr = req.Split('/');
        CalculateRequest reqModel = new CalculateRequest
        {
            FirstName = reqArr[0],
            LastName = reqArr[1],
            BasicSalary = Convert.ToDecimal(reqArr[2]),
            Allowance = Convert.ToDecimal(reqArr[3]),
            Transportation = Convert.ToDecimal(reqArr[4]),
            Date = DateHelper.GenerateDateFromShamsiDate(reqArr[5]),
            ShamsiDate = DateHelper.FormatShamsiDate(reqArr[5])
        };

        return reqModel;
    }

    private decimal CalculateBasedOnMethodName(string method, decimal BasicSalary, decimal Allowance)
    {
        switch (method)
        {
            case "CalcurlatorA":
                return _calculator.CalcurlatorA(BasicSalary, Allowance);
            case "CalcurlatorB":
                return _calculator.CalcurlatorB(BasicSalary, Allowance);
            case "CalcurlatorC":
                return _calculator.CalcurlatorC(BasicSalary, Allowance);
            default:
                return _calculator.CalcurlatorA(BasicSalary, Allowance);
        }
    }
}
